#!/bin/sh

# Work around resolver failure in debos' fakemachine
mv /etc/resolv.conf /etc/resolv2.conf
echo "nameserver 1.1.1.1" > /etc/resolv.conf

export DEBIAN_FRONTEND=noninteractive
export DEBCONF_NONINTERACTIVE_SEEN=true

echo "deb http://repo.ubports.com/ PR_mir_1 main" >> /etc/apt/sources.list.d/ubports.list

echo "" >> /etc/apt/preferences.d/ubports.pref
echo "Package: *" >> /etc/apt/preferences.d/ubports.pref
echo "Pin: origin repo.ubports.com" >> /etc/apt/preferences.d/ubports.pref
echo "Pin: release o=UBports,a=PR_mir_1" >> /etc/apt/preferences.d/ubports.pref
echo "Pin-Priority: 2001" >> /etc/apt/preferences.d/ubports.pref

if [ -n "$APT_PROXY" ]; then
    alias apt-get="apt-get -o Acquire::http::Proxy='$APT_PROXY'"
fi

apt-get update
apt-get dist-upgrade -y --allow-downgrades
apt-get autoremove -y

apt-get install ubuntu-touch-session-mir libgbm1 libgl1-mesa-dri hfd-service libqt5feedback5-hfd mesa-v*-drivers libegl-mesa0 mir-client-platform-mesa5 mir-client-platform-mesa-dev mir-platform-graphics-mesa-kms16 mir*graphics*desktop -y

# Undo changes to work around debos fakemachine resolver
rm /etc/resolv.conf
mv /etc/resolv2.conf /etc/resolv.conf
